<?php

namespace Drupal\Tests\redirect_deleted_entities\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the Redirect Deleted Entities module install without errors.
 *
 * @group redirect_deleted_entities
 */
class RedirectDeletedEntitiesInstallTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['redirect_deleted_entities'];

  /**
   * Assert that the redirect_deleted_entities module installed correctly.
   */
  public function testModuleInstalls() {
    // If we get here, then the module was successfully installed during the
    // setUp phase without throwing any Exceptions. Assert that TRUE is true,
    // so at least one assertion runs, and then exit.
    $this->assertTrue(TRUE, 'Module installed correctly.');
  }

}
