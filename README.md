## Redirect Deleted Entities

Sometimes there might be common patterns how deleted entities could be
redirected automatically. This module allows you to create redirects
when entity is being deleted. You can either use static path or tokens.
Redirects are configurable per entity bundle and language.

### Dependencies

This module requires the token and redirect modules.

* Token - https://www.drupal.org/project/token
* Redirect - https://www.drupal.org/project/redirect
